/*
   Appellation: currencies <module>
   Contributors: FL03 <jo3mccain@icloud.com>
   Description:
       ... Summary ...
*/
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum FungibleToken {
    ERC20 {
        name: String,
        symbol: String,
        total_supply: usize,
    },
}
