/*
   Appellation: clients <module>
   Contributors: FL03 <jo3mccain@icloud.com>
   Description:
       ... Summary ...
*/
use reqwest::{
    header::{HeaderMap, HeaderValue},
    Response,
};
use scsys::core::{BoxResult, Error};
use serde::{Deserialize, Serialize};
use strum::{EnumString, EnumVariantNames};

#[derive(
    Clone, Debug, Deserialize, EnumString, EnumVariantNames, Eq, Hash, PartialEq, Serialize,
)]
#[strum(serialize_all = "snake_case")]
pub enum State {
    Authorizing,
    Off,
    On,
}

impl Default for State {
    fn default() -> Self {
        Self::Off
    }
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct OAuth2 {
    pub id: String,
    pub secret: String,
}

impl OAuth2 {
    pub fn new(id: String, secret: String) -> Self {
        Self { id, secret }
    }
    pub fn from_env() -> Self {
        let id = match std::env::var_os("CLIENT_ID") {
            Some(v) => v.into_string().ok().unwrap(),
            None => panic!("{:?}", Error::default()),
        };
        let secret = match std::env::var_os("CLIENT_ID") {
            Some(v) => v.into_string().ok().unwrap(),
            None => panic!("{:?}", Error::default()),
        };
        Self::new(id, secret)
    }
}

impl Default for OAuth2 {
    fn default() -> Self {
        Self::new(String::new(), String::new())
    }
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct CbCloud {
    pub auth: Option<OAuth2>,
    pub state: State,
}

impl CbCloud {
    pub fn new(auth: Option<OAuth2>, state: Option<&str>) -> Self {
        let state = match state {
            Some(v) => match State::try_from(v) {
                Ok(v) => v,
                Err(_) => State::default(),
            },
            None => State::default(),
        };
        Self { auth, state }
    }

    pub fn client(&self) -> reqwest::Client {
        reqwest::Client::new()
    }

    pub async fn fetch(&self, endpoint: &str, path: Option<&str>) -> BoxResult<Response> {
        let headers = vec![
            ("Accept", "application/json"),
            ("User-Agent", crate::ME_USER_AGENT),
        ];
        let path = match path {
            Some(v) => v,
            None => "",
        };
        let response = self
            .client()
            .get(extend_endpoint(endpoint, path))
            .headers(header_map_from_vec(headers))
            .send()
            .await?;
        Ok(response)
    }

    pub async fn fetch_json(
        &self,
        endpoint: &str,
        path: Option<&str>,
    ) -> BoxResult<serde_json::Value> {
        let res = self
            .fetch(endpoint, path)
            .await
            .expect("")
            .json()
            .await
            .expect("Response Error");
        Ok(res)
    }

    pub async fn currencies(&self) -> BoxResult<serde_json::Value> {
        self.fetch_json(crate::EXCHANGE_URL, Some("currencies"))
            .await
    }
}

pub fn header_map_from_vec(data: Vec<(&'static str, &'static str)>) -> HeaderMap {
    let mut headers = HeaderMap::new();
    for i in data {
        headers.append(i.0, HeaderValue::from_static(i.1));
    }
    headers
}

pub fn extend_endpoint(endpoint: &str, path: &str) -> reqwest::Url {
    let data = format!("{}{}", endpoint.to_string(), path);
    reqwest::Url::parse(data.as_str()).expect("")
}
