/*
   Appellation: core <module>
   Contributors: FL03 <jo3mccain@icloud.com>
   Description:
       ... Summary ...
*/
pub use self::{clients::*, primitives::*};

mod clients;
mod primitives;
