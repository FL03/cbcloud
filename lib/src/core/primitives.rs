/*
   Appellation: primitives <module>
   Contributors: FL03 <jo3mccain@icloud.com>
   Description:
       ... Summary ...
*/
pub use self::{constants::*, statics::*, types::*};

mod constants {
    pub const EXCHANGE_URL: &str = "https://api.exchange.coinbase.com";
    pub const AUTHORIZE_URL: &str = "https://www.coinbase.com/oauth/authorize";
    pub const ACCESS_TOKEN_URL: &str = "https://www.coinbase.com/oauth/token";

    pub const ME_USER_AGENT: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.59";
}

mod statics {}

mod types {}
