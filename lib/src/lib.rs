/*
   Appellation: cbcloud <module>
   Contributors: FL03 <jo3mccain@icloud.com>
   Description:
       ... Summary ...
*/
#[doc(inline)]
pub use self::{actors::*, core::*, data::*};

pub(crate) mod actors;
pub(crate) mod core;
pub(crate) mod data;

mod examples {
    use oauth2::basic::BasicClient;
    use oauth2::reqwest::http_client;
    use oauth2::{
        AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, PkceCodeChallenge,
        RedirectUrl, Scope, TokenResponse, TokenUrl,
    };
    use scsys::core::BoxResult;

    pub fn example(
        id: String,
        secret: String,
        authorize_url: String,
        token_url: String,
        redirect_url: String,
    ) -> BoxResult {
        let client = BasicClient::new(
            ClientId::new(id),
            Some(ClientSecret::new(secret)),
            AuthUrl::new(authorize_url)?,
            Some(TokenUrl::new(token_url)?),
        )
        // Set the URL the user will be redirected to after the authorization process.
        .set_redirect_uri(RedirectUrl::new(redirect_url)?);

        // Generate a PKCE challenge.
        let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

        // Generate the full authorization URL.
        let (auth_url, _csrf_token) = client
            .authorize_url(CsrfToken::new_random)
            // Set the desired scopes.
            .add_scope(Scope::new("read".to_string()))
            .add_scope(Scope::new("write".to_string()))
            // Set the PKCE code challenge.
            .set_pkce_challenge(pkce_challenge)
            .url();

        // This is the URL you should redirect the user to, in order to trigger the authorization
        // process.
        println!("Browse to: {}", auth_url);

        let mut buffer = String::new();
        let stdin = std::io::stdin().read_line(&mut buffer)?.to_string();

        let _token_result = client
            .exchange_code(AuthorizationCode::new(stdin))
            // Set the PKCE code verifier.
            .set_pkce_verifier(pkce_verifier)
            .request(http_client)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_client() {
        let a = CbCloud::new(Some(OAuth2::default()), None);
        let b = a.clone();
        assert_eq!(a, b)
    }
}
