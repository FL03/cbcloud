/*
   Appellation: clients <module>
   Contributors: FL03 <jo3mccain@icloud.com>
   Description:
       ... Summary ...
*/
pub use self::{auth::*, public::*};

pub(crate) mod auth;
pub(crate) mod public;
